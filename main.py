#!/usr/bin/python
import argparse
import re
import string
from collections import OrderedDict


def process_file(file):
    words = {}
    for line in file:
        word = line.translate(string.maketrans("",""), string.punctuation).lower().strip()
        if word in words:
            words[word] += 1
        else:
            words[word] = 1
    return OrderedDict(sorted(
        {k: v for k, v in words.iteritems() if v > 1}.items(), key=lambda t: t[1], reverse=True))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process string.')
    parser.add_argument('--file', type=argparse.FileType('r'),
        help='File for parsing', required=True)
    args = parser.parse_args()
    file = args.file
    for k, v in process_file(file).items():
        print k, ':', v
